# gradle-simple

[![](https://jitpack.io/v/org.bitbucket.punicappinternal/snodnoklassniki.svg)](https://jitpack.io/#org.bitbucket.punicappinternal/snodnoklassniki)

Punicapp odnoklassniki module library, which is used in internal projects.

To install the library add: 
 
```
   allprojects {
       repositories {
           maven { url 'https://jitpack.io' }
       }
   }
   dependencies {
           compile 'org.bitbucket.punicappinternal:snodnoklassniki:1.0.1'
   }
```