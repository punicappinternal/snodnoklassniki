package com.punicapp.snodnoklassniki;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.punicapp.library.OkSNConnector;
import com.punicapp.sncore.SNAccessToken;
import com.punicapp.sncore.SNManager;
import com.punicapp.sncore.SNType;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DefaultObserver;

public class MainActivity extends AppCompatActivity {

    private SNManager sManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SNManager.Builder builder = new SNManager.Builder(this);
        sManager = builder.with(new OkSNConnector(this, "1262594304", "CBAKKCEMEBABABABA"))
                .build();

        findViewById(R.id.login_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Observable<SNAccessToken> auth = sManager.auth(SNType.OK);
                auth.subscribe(new DefaultObserver<SNAccessToken>() {

                    @Override
                    public void onNext(@NonNull SNAccessToken snAccessToken) {
                        showDialog(snAccessToken.toString());
                        cancel();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        showDialog("ERRROR: " + e.getMessage());
                        cancel();
                    }

                    @Override
                    public void onComplete() {
                        cancel();
                    }
                });
            }
        });


    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(message)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }
}
