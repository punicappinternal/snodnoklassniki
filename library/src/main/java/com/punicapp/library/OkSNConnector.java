package com.punicapp.library;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Pair;

import com.punicapp.sncore.ISNConnector;
import com.punicapp.sncore.SNAccessToken;
import com.punicapp.sncore.SNType;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observer;
import io.reactivex.subjects.PublishSubject;
import ru.ok.android.sdk.Odnoklassniki;
import ru.ok.android.sdk.OkAuthListener;
import ru.ok.android.sdk.OkListener;
import ru.ok.android.sdk.util.OkAuthType;
import ru.ok.android.sdk.util.OkScope;

public class OkSNConnector implements ISNConnector {
    private static final String ACCESS_TOKEN = "access_token";
    private static final String SESSION_SECRET_KEY = "session_secret_key";
    private static final String UID = "uid";
    private static final String USERS_GET_CURRENT_USER = "users.getCurrentUser";
    private static final String OK = "ok";
    private static final String AUTHORIZE = "://authorize";
    private static final String EMPTY_TOKENS = "EMPTY TOKENS";
    public static final String CANCEL = "Cancel";

    private final Odnoklassniki odnoklassniki;

    private Observer<SNAccessToken> observer;

    private OkAuthListener okListener = new OkAuthListener() {
        @Override
        public void onSuccess(final JSONObject json) {
            getToken(json);
        }

        @Override
        public void onError(String error) {
            observer.onError(new Throwable(error));
            observer.onComplete();
        }

        @Override
        public void onCancel(String error) {
            observer.onError(new Throwable(CANCEL));
            observer.onComplete();
        }
    };

    protected String REDIRECT_URL;

    public OkSNConnector(Context context, String appId, String appKey) {
        REDIRECT_URL = OK + appId + AUTHORIZE;
        odnoklassniki = Odnoklassniki.createInstance(context, appId, appKey);
    }

    @Override
    public SNType getType() {
        return SNType.OK;
    }

    @Override
    public boolean requestAuth(final Activity shadowActivity) {
        OkListener requestAuthListener = new OkListener() {
            @Override
            public void onSuccess(JSONObject json) {
                getToken(json);
                shadowActivity.finish();
            }

            @Override
            public void onError(String error) {
                odnoklassniki.requestAuthorization(shadowActivity, REDIRECT_URL, OkAuthType.ANY, OkScope.VALUABLE_ACCESS);
            }
        };

        odnoklassniki.checkValidTokens(requestAuthListener);

        return false;
    }

    private void getToken(JSONObject json) {
        Pair<String, String> token = parseTokens(json);
        if (token == null) {
            observer.onError(new Throwable(EMPTY_TOKENS));
            observer.onComplete();
            return;
        }
        getUser(token.first, token.second);
    }

    private Pair<String, String> parseTokens(JSONObject json) {
        String accessToken = null;
        String secret = null;
        try {
            if (json.has(ACCESS_TOKEN)) {
                accessToken = json.getString(ACCESS_TOKEN);
            }
            if (json.has(SESSION_SECRET_KEY)) {
                secret = json.getString(SESSION_SECRET_KEY);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            observer.onError(new Throwable(e));
            observer.onComplete();
            return null;
        }

        return new Pair<>(accessToken, secret);
    }

    private void getUser(final String accessToken, final String secret) {
        OkListener getUserListener = new OkListener() {
            @Override
            public void onSuccess(JSONObject json) {
                if (json.has(UID)) {
                    String uid = null;
                    try {
                        uid = json.getString(UID);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        observer.onError(new Throwable(EMPTY_TOKENS));
                        observer.onComplete();
                    }
                    observer.onNext(new SNAccessToken(accessToken, secret, uid));
                    observer.onComplete();
                }
            }

            @Override
            public void onError(String error) {
                observer.onError(new Throwable(error));
                observer.onComplete();
            }
        };
        odnoklassniki.requestAsync(USERS_GET_CURRENT_USER, null, null, getUserListener);
    }

    @Override
    public void handleActivityResult(int requestCode, int resultCode, Intent data) {
        if (Odnoklassniki.getInstance().isActivityRequestOAuth(requestCode)) {
            Odnoklassniki.getInstance().onAuthActivityResult(requestCode, resultCode, data, okListener);
        }
    }

    @Override
    public void bindAuthCallback(PublishSubject<SNAccessToken> observer) {
        this.observer = observer;
    }
}
